<?php

namespace SMSBump\Models;

class User
{
    public int $id;
    public string $email;
    public string $phone;
    public string $otp;
    public int $attempts;
    public string $last_attempt;
    public string $verification_sent;
    public string $verified;

    public function __construct(string $email, string $phone, string $otp)
    {
        $this->email = $email;
        $this->phone = $phone;
        $this->otp = $otp;
    }
}
