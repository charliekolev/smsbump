<?php

namespace SMSBump\Models\Repositories;

use SMSBump\Lib\Generic\AbstractDbRepository;
use SMSBump\Models\User;

class UserDBRepository extends AbstractDbRepository implements UserRepositoryInterface
{
    public function getTableName(): string
    {
        return 'users';
    }

    /**
     * @inheritDoc
     */
    public function crete(User $user): void
    {
        $this->db->query(
            "INSERT INTO `$this->table` (`email`, `phone`, `otp`, `verification_sent`) 
                VALUES (:email, :phone, :otp, NOW())",
            [
                'email' => $user->email,
                'phone' => $user->phone,
                'otp' => $user->otp,
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function lastInsertId()
    {
        return $this->db->lastInsertId();
    }

    /**
     * @inheritDoc
     */
    public function hasByEmail(string $email): bool
    {
        return (bool)$this->db
            ->query("SELECT 1 FROM `$this->table` WHERE `email` = :email", ['email' => $email])
            ->fetch();
    }

    /**
     * @inheritDoc
     */
    public function getVerificationDateById(int $id): ?string
    {
        return (
            $user = $this->db
                ->query("SELECT `verification_sent` FROM `$this->table` WHERE `id` = :id", ['id' => $id])
                ->fetch()
        ) ? $user['verification_sent'] : null;
    }

    /**
     * @inheritDoc
     */
    public function setNewPass(int $id, string $pass): void
    {
        $this->db->query(
            "UPDATE `$this->table`
                SET `otp` = :otp, `attempts` = 0, `last_attempt` = NULL, `verification_sent` = NOW()
                WHERE id = :id",
            [
                'id' => $id,
                'otp' => $pass,
            ]
        );
    }
}
