<?php

namespace SMSBump\Models\Repositories;

use SMSBump\Models\User;

interface UserRepositoryInterface
{
    /**
     * Create new user
     *
     * @param User $user
     *
     * @return void
     */
    public function crete(User $user): void;

    /**
     * Get last inserted user ID
     *
     * @return false|string
     */
    public function lastInsertId();

    /**
     * Check if user exists by his email
     *
     * @param string $email
     *
     * @return bool
     */
    public function hasByEmail(string $email): bool;

    /**
     * Get Verification sent date
     *
     * @param int $id
     * @return string|null
     */
    public function getVerificationDateById(int $id): ?string;

    /**
     * Set new password
     *
     * @param int $id
     * @param string $pass
     *
     * @return void
     */
    public function setNewPass(int $id, string $pass): void;
}
