<?php

namespace SMSBump\Lib\DB;

use PDO;
use PDOStatement;

class Database
{
    private PDO $pdo;

    /**
     * @param PDO $dbh
     */
    public function __construct(PDO $dbh)
    {
        $this->pdo = $dbh;
    }

    /**
     * Execute a DB query.
     *
     * @param string $sql
     * @param array|null $data
     *
     * @return PDOStatement|false
     */
    public function query(string $sql, array $data = null)
    {
        if (!$data) {
            return $this->pdo->query($sql);
        }

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($data);
        return $stmt;
    }

    /**
     * Get last inserted id.
     *
     * @return string|false
     */
    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();
    }
}
