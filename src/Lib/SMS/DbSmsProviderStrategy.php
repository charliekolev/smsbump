<?php

namespace SMSBump\Lib\SMS;

use SMSBump\Lib\DB\Database;

/**
 * This class mocks sending of SMS messages into the DB
 */
class DbSmsProviderStrategy implements SmsProviderStrategyInterface
{
    private Database $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    /**
     * Create an SMS log in DB
     *
     * @param string $phoneNumber
     * @param string $message
     *
     * @return bool
     */
    public function send(string $phoneNumber, string $message = ''): bool
    {
        return (bool)$this->db->query(
            'INSERT INTO `sms_logs` (`phone`, `message`, `sent`) VALUES (:phone, :message, NOW())',
            [
                'phone' => $phoneNumber,
                'message' => $message,
            ]
        );
    }
}
