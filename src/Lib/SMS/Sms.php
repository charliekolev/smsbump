<?php

namespace SMSBump\Lib\SMS;

/**
 * Sends SMS messages to the specified provider
 */
class Sms
{
    private SmsProviderStrategyInterface $provider;

    /**
     * @param SmsProviderStrategyInterface $provider
     */
    public function __construct(SmsProviderStrategyInterface $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Send SMS message to any provider
     * that implements SmsProviderStrategyInterface interface
     *
     * @param string $phoneNumber
     * @param string $message
     *
     * @return bool
     */
    public function send(string $phoneNumber, string $message): bool
    {
        /*
         * Here we can put some cool logic, like filtering or trimming the message,
         * if there are some SMS standards to follow.
         */
        return $this->provider->send($phoneNumber, $message);
    }
}
