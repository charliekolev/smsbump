<?php

namespace SMSBump\Lib\SMS;

/**
 * An interface to be implemented by all the SMS providers
 */
interface SmsProviderStrategyInterface
{
    /**
     * Send SMS message
     *
     * @param string $phoneNumber
     * @param string $message
     *
     * @return bool
     */
    public function send(string $phoneNumber, string $message = ''): bool;
}
