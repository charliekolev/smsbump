<?php

namespace SMSBump\Lib\DependencyInjection;

use Exception;

class DependencyNotFoundException extends Exception
{
}
