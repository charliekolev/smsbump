<?php

namespace SMSBump\Lib\DependencyInjection;

/**
 * The dependency container Singleton class.
 */
class DependencyContainer
{
    /**
     * The singleton instance of this class
     * @var DependencyContainer
     */
    private static DependencyContainer $instance;

    /**
     * The collection of dependencies contained.
     * @var Dependency[]
     */
    private array $dependencies;

    private function __construct()
    {
        $this->dependencies = [];
    }

    public static function getInstance(): DependencyContainer
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Adds an object generator and the identifier for that object.
     *
     * @param string $identifier
     *  The identifier of the dependency
     * @param callable $loader
     *  The generator for the dependency object
     * @param bool $singleton
     *  Whether to return always the same instance of the object
     */
    public function add(string $identifier, callable $loader, bool $singleton = true): void
    {
        $this->dependencies[$identifier] = new Dependency($loader, $singleton);
    }

    /**
     * Gets the dependency by its identifier.
     *
     * @param string $identifier
     *  The identifier of the dependency
     *
     * @return object
     *  The object identified by the given id
     * @throws DependencyNotFoundException
     */
    public function get(string $identifier): object
    {
        if (!isset($this->dependencies[$identifier])) {
            throw new DependencyNotFoundException("Dependency identified by '$identifier' does not exist");
        }

        return $this->dependencies[$identifier]->get();
    }
}
