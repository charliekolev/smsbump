<?php

namespace SMSBump\Lib\DependencyInjection;

use ReflectionClass;
use ReflectionException;

/**
 * This class is just a wrapper to avoid the use a singleton class.
 */
class DependencyInjectionService
{
    private DependencyContainer $container;

    public function __construct()
    {
        $this->container = DependencyContainer::getInstance();
    }

    /**
     * Registers a dependency into the dependency container
     *
     * @param string $identifier
     *  The identifier of the dependency
     * @param callable $loader
     *  The generator for the dependency object
     * @param bool $singleton
     *  Whether to return always the same instance of the object
     *
     * @return void
     */
    public function register(string $identifier, callable $loader, bool $singleton = true): void
    {
        $this->container->add($identifier, $loader, $singleton);
    }

    /**
     * Gets the dependency by its identifier.
     *
     * @param string $identifier
     *  The identifier of the dependency
     *
     * @return object
     * @throws DependencyNotFoundException
     */
    public function get(string $identifier): object
    {
        return $this->container->get($identifier);
    }

    /**
     * Find dependencies and resolve an instance
     *
     * @param string $identifier
     *
     * @return object
     * @throws ReflectionException
     */
    public function resolve(string $identifier): object
    {
        // If $class is an abstraction bound in the injection service
        try {
            return $this->get($identifier);
        } catch (DependencyNotFoundException $e) {
            // Not found as a dependency - continue resolving as a class
        }

        $reflectionClass = new ReflectionClass($identifier);

        if (
            ($constructor = $reflectionClass->getConstructor()) === null ||
            ($params = $constructor->getParameters()) === []
        ) {
            return $reflectionClass->newInstance();
        }

        $newInstanceParams = [];

        foreach ($params as $param) {
            $newInstanceParams[] = $param->getClass() === null
                ? $param->getDefaultValue()
                : $this->resolve($param->getClass()->getName());
        }

        return $reflectionClass->newInstanceArgs($newInstanceParams);
    }
}
