<?php

namespace SMSBump\Lib\Generic;

use PDO;
use SMSBump\Lib\DB\Database;

abstract class AbstractDbRepository
{
    protected Database $db;
    protected string $table;

    /**
     * @param Database $db
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
        $this->table = $this->getTableName();
    }

    abstract public function getTableName(): string;

    /**
     * Get table row count
     *
     * @return int
     */
    public function count(): int
    {
        return $this->db->query("SELECT count(*) from `$this->table`")->fetchColumn();
    }

    /**
     * Get all records
     *
     * @return array
     */
    public function getAll(): array
    {
        return $this->db->query("SELECT * from `$this->table`")->fetchAll(PDO::FETCH_ASSOC);
    }
}
