<?php

namespace SMSBump\Lib\Generic;

class Session
{
    /**
     * Start the session
     */
    public function start()
    {
        session_start();
    }

    /**
     * Get a session by key
     *
     * @param string $key
     *
     * @return mixed|null
     */
    public function get(string $key)
    {
        return $_SESSION[$key] ?? null;
    }

    /**
     * Get the session
     *
     * @return array|null
     */
    public function getAll(): ?array
    {
        return $_SESSION;
    }

    /**
     * Set a session variable
     *
     * @param string $key
     * @param $value
     *
     * @return void
     */
    public function set(string $key, $value): void
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Remove all session keys
     *
     * @return void
     */
    public function clear(): void
    {
        $_SESSION = [];
    }
}
