<?php

namespace SMSBump\Lib\Generic;

use ReflectionException;
use SMSBump\Lib\DB\Database;
use SMSBump\Lib\DependencyInjection\DependencyInjectionService;
use SMSBump\Lib\DependencyInjection\DependencyNotFoundException;
use SMSBump\Lib\Generic\Exceptions\ControllerException;
use SMSBump\Lib\SMS\Sms;

/**
 * This is the base controller class
 */
abstract class AbstractController
{
    /**
     * Get the configuration object
     *
     * @return object
     * @throws ControllerException
     */
    protected function config(): object
    {
        try {
            return (new DependencyInjectionService())->get('config');
        } catch (DependencyNotFoundException $e) {
            throw new ControllerException($e->getMessage());
        }
    }

    /**
     * Get the session object
     *
     * @return Session
     * @throws ControllerException
     */
    protected function session(): Session
    {
        try {
            return (new DependencyInjectionService())->get('session');
        } catch (DependencyNotFoundException $e) {
            throw new ControllerException($e->getMessage());
        }
    }

    /**
     * Get the Database object
     *
     * @return Database
     * @throws ControllerException
     */
    protected function db(): Database
    {
        return $this->resolve(Database::class);
    }

    /**
     * Get the SMS provider object
     *
     * @return Sms
     * @throws ControllerException
     */
    protected function sms(): Sms
    {
        return $this->resolve(Sms::class);
    }

    /**
     * Get an object from dependency container
     *
     * @param string $identifier
     *
     * @return object
     * @throws ControllerException
     */
    protected function resolve(string $identifier): object
    {
        try {
            return (new DependencyInjectionService())->resolve($identifier);
        } catch (ReflectionException $e) {
            throw new ControllerException($e->getMessage());
        }
    }
}
