<?php

namespace SMSBump\Lib\Generic\Exceptions;

use Exception;

/**
 * Exception that defines whether this exception should be visible for the users
 * or just for the administration
 */
class ControllerException extends Exception
{
    public const VISIBILITY_ADMINISTRATOR = 0;
    public const VISIBILITY_USER = 1;

    public function __construct(string $message, int $code = self::VISIBILITY_ADMINISTRATOR)
    {
        parent::__construct($message, $code);
    }
}
