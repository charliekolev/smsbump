<?php

namespace SMSBump\Lib\Generic;

class View
{
    /**
     * Render the content of a template.
     *
     * @param string $template
     * @param $data
     *
     * @return void
     */
    public static function render(string $template, $data = []): void
    {
        if (is_array($data)) {
            $viewFile = VIEWS_DIRECTORY . DIRECTORY_SEPARATOR . $template . '.php';

            if (!file_exists($viewFile)) {
                smsbump_error_page('Not found!', 404);
            }

            // Show the HTML
            require VIEWS_DIRECTORY . DIRECTORY_SEPARATOR . 'Main' . DIRECTORY_SEPARATOR . 'page.php';
        } elseif (is_string($data)) {
            echo $data;
        } elseif (!is_null($data)) {
            smsbump_error_page('Controller returns wrong data!', 500);
        }
    }
}
