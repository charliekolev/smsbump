<header>
    <h1>ERROR <?php echo $data['error'] ?? ''; ?></h1>
</header>
<section>
    <div id="container_demo" >
        <div id="wrapper">
            <div id="register">
                <p><?php echo $data['message'] ?? ''; ?></p>
                <p class="change_link"></p>
            </div>
        </div>
    </div>
</section>
