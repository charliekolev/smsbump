<header>
    <h1>This is SMSBump home page</h1>
</header>
<section>
    <div id="container_demo" >
        <div id="wrapper">
            <div id="register">
                <h1>Welcome to SMSBump</h1>
                <?php if (!empty($data['verified'])): ?>
                    <h2>You are logged-in user!</h2>
                    <p class="change_link">
                        <a href="/?section=authentication&action=logout">Logout</a>
                    </p>
                <?php else: ?>
                    <h2>You are not logged-in user!</h2>
                    <p class="change_link">
                        Not a member yet ?
                        <a href="/?section=authentication&action=register">Join us</a>
                    </p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
