<header>
    <h1>Registration Form</h1>
</header>
<section>
    <div id="container_demo" >
        <div id="wrapper">
            <div id="register" class="form">
                <form name="register" method="post" action="">
                    <h1> Sign up </h1>
                    <?php if (in_array($data['step'], [1, 2], true)): ?>
                        <p class="<?php if (!empty($data['errors']['email'])) { echo 'alert'; } ?> required">
                            <label for="email">Your email</label>
                            <input
                                id="email"
                                name="email"
                                type="email"
                                placeholder="mysupermail@mail.com"
                                value="<?php echo $data['email'] ?? ''; ?>"
                                <?php if ($data['step'] === 2) { echo 'disabled'; } ?>
                            />
                            <?php if (!empty($data['errors']['email'])): ?>
                                <span class="alert-validation" aria-hidden="true">
                                    <?php echo $data['errors']['email']; ?>
                                </span>
                            <?php endif; ?>
                        </p>
                        <p class="<?php if (!empty($data['errors']['phone'])) { echo 'alert'; } ?> required">
                            <label for="phone">Your mobile phone number</label>
                            <input
                                id="phone"
                                name="phone"
                                type="tel"
                                placeholder="+359 123 456789"
                                value="<?php echo $data['phone'] ?? ''; ?>"
                                <?php if ($data['step'] === 2) { echo 'disabled'; } ?>
                            />
                            <?php if (!empty($data['errors']['phone'])): ?>
                                <span class="alert-validation" aria-hidden="true">
                                    <?php echo $data['errors']['phone']; ?>
                                </span>
                            <?php endif; ?>
                        </p>
                        <?php if ($data['step'] === 1): ?>
                            <p class="signin button">
                                <input type="submit" name="register" value="Verify me" />
                            </p>
                        <?php elseif ($data['step'] === 2): ?>
                            <p>
                                We have sent a message with a password to your mobile phone.
                                Use it in the field below, in order to verify its you.
                            </p>
                            <p
                                class="<?php
                                    if (!empty($data['errors']['new_code'])) { echo 'alert'; }
                                    elseif (!empty($data['success']['new_code'])) { { echo 'alert alert-ok'; } }
                                ?>"
                            >
                                If you need a new verification code
                                <input type="submit" name="new_code" class="link-button" value="click here" />
                                <?php if (!empty($data['errors']['new_code']) || !empty($data['success']['new_code'])): ?>
                                    <span class="alert-validation" aria-hidden="true">
                                        <?php echo !empty($data['errors']['new_code'])
                                            ? $data['errors']['new_code']
                                            : $data['success']['new_code'];
                                        ?>
                                    </span>
                                <?php endif; ?>
                            </p>
                            <p class="<?php if (!empty($data['errors']['password'])) { echo 'alert'; } ?> required">
                                <label for="password">Password</label>
                                <input id="password" name="password" type="password" />
                                <?php if (!empty($data['errors']['password'])): ?>
                                    <span class="alert-validation" aria-hidden="true">
                                        <?php echo $data['errors']['password']; ?>
                                    </span>
                                <?php endif; ?>
                            </p>
                            <p class="signin button">
                                <input type="submit" name="register" value="Sign up" />
                            </p>
                        <?php endif; ?>
                    <?php elseif ($data['step'] === 3): ?>
                        <h2>
                            You have been signed-up successfully!
                            You are now logged-in and ready to browse our pages.
                        </h2>
                    <?php elseif ($data['step'] === 4): ?>
                        <h2>
                            You are logged-in user! If you want to logout
                            <a href="/?section=authentication&action=logout">click here</a>
                        </h2>
                    <?php endif; ?>
                    <p class="change_link">
                        Go back to <a href="/">Home page</a>
                    </p>
                </form>
            </div>

            <?php if ($data['step'] === 2): ?>
                <div class="smartphone">
                    <div class="content">
                        <div class="message">
                            <p>NEW MESSAGE:</p>
                            <p><?php echo $data['sms_log']['message'] ?? 'Oops! Something is wrong.' ?></p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
