<?php

namespace SMSBump\Controllers;

use DateTime;
use SMSBump\Lib\Generic\AbstractController;
use SMSBump\Lib\Generic\Exceptions\ControllerException;
use SMSBump\Models\Repositories\UserRepositoryInterface;
use SMSBump\Models\User;

class AuthenticationController extends AbstractController
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Register action
     *
     * @return array
     * @throws ControllerException
     */
    public function register(): array
    {
        $data = [];
        $session = $this->session();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (!$session->get('id')) {
                // STEP 1 - email and phone fields

                // Validation
                $data['email'] = $_POST['email'] ?? '';
                $data['phone'] = $_POST['phone'] ?? '';

                if (empty($data['email'])) {
                    $data['errors']['email'] = 'Email field is mandatory';
                } elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                    $data['errors']['email'] = 'Wrong email format';
                } elseif ($this->userRepository->hasByEmail($data['email'])) {
                    $data['errors']['email'] = 'User with that email already exists';
                }

                if (empty($data['phone'])) {
                    $data['errors']['phone'] = 'Mobile number field is mandatory';
                } else {
                    $data['fixed_phone'] = preg_replace('/[^0-9]+/', '', $data['phone']);

                    if (empty($data['fixed_phone'])) {
                        $data['errors']['phone'] = 'The mobile number should contain digits';
                    } else {
                        if (strpos($data['fixed_phone'], '359') !== 0) {
                            if ($data['fixed_phone'][0] === '0') {
                                $data['fixed_phone'] = substr($data['fixed_phone'], 1);
                            } else {
                                $data['errors']['phone'] = 'Wrong mobile number format';
                            }

                            $data['fixed_phone'] = '359' . $data['fixed_phone'];
                        }

                        if (strlen($data['fixed_phone']) !== 12) {
                            $data['errors']['phone'] = 'Wrong mobile number format';
                        }
                    }
                }

                // Validation passed - create user and send SMS message with validation code
                if (empty($data['errors'])) {
                    $pass = $this->generatePassword();

                    // Create the user in DB
                    $this->userRepository->crete(new User($data['email'], $data['fixed_phone'], $pass));

                    if ($data['id'] = $this->userRepository->lastInsertId()) {
                        $session->set('id', $data['id']);

                        foreach (['email', 'phone', 'fixed_phone'] as $field) {
                            $session->set($field, $data[$field]);
                        }
                    } else {
                        throw new ControllerException('We have encountered some issues when cheating the user.');
                    }

                    $data['sms_log']['message'] = $this->sendPasswordSms($data['fixed_phone'], $pass);
                }
            } else {
                // STEP 2 - verify password

                // We are not taking the POST data anymore to show these fields
                foreach (['id', 'email', 'phone', 'fixed_phone'] as $field) {
                    $data[$field] = $session->get($field);
                }

                // TODO - all $db instances to be replaced with $this->userRepository !!!
                $db = $this->db();

                // Resend a new password
                if (!empty($_POST['new_code'])) {
                    if (
                        new DateTime(
                            $this->userRepository->getVerificationDateById($data['id'])
                        ) < (new DateTime())->modify('-1 minute')
                    ) {
                        $pass = $this->generatePassword();
                        $this->userRepository->setNewPass($data['id'], $pass);

                        $data['sms_log']['message'] = $this->sendPasswordSms($data['fixed_phone'], $pass);
                        $data['success']['new_code'] = 'New password was sent';
                    } else {
                        $data['errors']['new_code'] = 'You need to wait 1 minute before sending a new code. Try later!';
                    }
                } elseif (!empty($_POST['password'])) {
                    $status = 'UNKNOWN';

                    $user = $db
                        ->query(
                            'SELECT `otp`, `attempts`, last_attempt FROM `users` WHERE `id` = :id',
                            ['id' => $data['id']]
                        )
                        ->fetch();

                    // This is the 3d attempt - 1 minute cool-down
                    if ($user['attempts'] >= 3) {
                        if (new DateTime($user['last_attempt']) < (new DateTime())->modify('-1 minute')) {
                            $user['attempts'] = 0;
                        } else {
                            $data['errors']['password'] = 'Too many attempts. Please try again after 1 minute.';
                            $status = 'AFTER LIMIT REACHED';
                        }
                    }

                    if ($status === 'UNKNOWN') {
                        if ($user['otp'] !== $_POST['password']) {
                            $data['errors']['password'] = 'Wrong password';

                            $db->query(
                                'UPDATE `users` SET `attempts` = :attempts, last_attempt = NOW() WHERE `id` = :id',
                                [
                                    'attempts' => ++$user['attempts'],
                                    'id' => $data['id']
                                ]
                            );

                            $status = 'WRONG CODE';
                        } else {
                            $db->query(
                                'UPDATE `users` 
                                    SET `attempts` = :attempts, `last_attempt` = NOW(), `verified` = NOW() 
                                    WHERE `id` = :id',
                                [
                                    'attempts' => ++$user['attempts'],
                                    'id' => $data['id']
                                ]
                            );

                            $config = $this->config();

                            $message = !empty($config->sms['welcome_message'])
                                ? $config->sms['welcome_message']
                                : 'Welcome to SMSBump!';

                            if (!$this->sms()->send($data['fixed_phone'], $message)) {
                                throw new ControllerException('We have encountered some issues with the SMS provider.');
                            }

                            $session->set('verified', 1);
                            $status = 'VERIFIED';
                            $justVerified = true;
                        }
                    }

                    // Create log entry for the attempt
                    $db->query(
                        'INSERT INTO `user_verification_logs` (`user_id`, `otp`, `status`, `created`) 
                            VALUES (:user_id, :otp, :status, NOW())',
                        [
                            'user_id' => $data['id'],
                            'otp' => $_POST['password'],
                            'status' => $status,
                        ]
                    );
                } else {
                    $data['errors']['password'] = 'Enter the password from the SMS';
                }

                // Take the last SMS message send to that phone number
                // and show it in the mobile phone visualization on the page.
                if (!isset($data['sms_log'])) {
                    $data['sms_log'] = $db
                        ->query(
                            'SELECT `message` FROM `sms_logs` WHERE `phone` = :phone ORDER by `sent` DESC LIMIT 1',
                            ['phone' => $data['fixed_phone']]
                        )
                        ->fetch();
                }
            }
        }

        if ($session->get('verified')) {
            $data['step'] = isset($justVerified) ? 3 : 4;
        } elseif ($session->get('id')) {
            $data['step'] = 2;
        } else {
            $data['step'] = 1;
        }

        return $data;
    }

    /**
     * Logout action
     *
     * @return void
     * @throws ControllerException
     */
    public function logout(): void
    {
        $this->session()->clear();
        header('Location: /');
    }

    /**
     * Password generator
     * @return string
     */
    private function generatePassword(): string
    {
        $comb = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $combLen = strlen($comb) - 1;
        $pass = '';

        for ($i = 0; $i < 8; $i++) {
            $pass .= $comb[rand(0, $combLen)];
        }

        return $pass;
    }

    /**
     * Send SMS with the password
     *
     * @param $phone
     * @param $pass
     *
     * @return string
     * @throws ControllerException
     */
    private function sendPasswordSms($phone, $pass): string
    {
        $config = $this->config();

        $message = !empty($config->sms['password_message'])
            ? str_replace('[[PASS]]', $pass, $config->sms['password_message'])
            : $pass;

        if (!$this->sms()->send($phone, $message)) {
            throw new ControllerException('We have encountered some issues with the SMS provider.');
        } else {
            return $message;
        }
    }
}
