<?php

namespace SMSBump\Controllers;

use SMSBump\Lib\Generic\AbstractController;
use SMSBump\Lib\Generic\Exceptions\ControllerException;

class HomeController extends AbstractController
{
    /**
     * @return array
     * @throws ControllerException
     */
    public function index(): array
    {
        return ['verified' => $this->session()->get('verified')];
    }
}
