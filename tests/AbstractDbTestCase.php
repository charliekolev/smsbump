<?php

namespace Tests;

use PDO;
use PHPUnit\Framework\TestCase;
use SMSBump\Lib\DB\Database;
use SMSBump\Lib\DependencyInjection\DependencyInjectionService;
use SMSBump\Lib\DependencyInjection\DependencyNotFoundException;

class AbstractDbTestCase extends TestCase
{
    private static ?Database $db;

    /**
     * Create db instance only once
     *
     * @return void
     * @throws DependencyNotFoundException
     */
    public static function setUpBeforeClass(): void
    {
        $injector = new DependencyInjectionService();
        self::$db = $injector->get(Database::class);
    }

    /**
     * Truncate DB tables for each test
     *
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();

        $tables = self::$db->query('SHOW TABLES');
        $tables->execute();

        foreach($tables->fetchAll(PDO::FETCH_COLUMN) as $table)
        {
            self::$db->query('DELETE FROM `' . $table . '` WHERE 1')->execute();
            self::$db->query('ALTER TABLE `' . $table . '` AUTO_INCREMENT = 1')->execute();
            //self::$db->query('TRUNCATE TABLE `' . $table . '`')->execute();
        }
    }

    /**
     * Remove the db instance
     *
     * @return void
     */
    public static function tearDownAfterClass(): void
    {
        self::$db = null;
    }
}
