<?php

use SMSBump\Lib\DependencyInjection\DependencyInjectionService;
use SMSBump\Models\Repositories\UserDBRepository;
use SMSBump\Models\User;
use Tests\AbstractDbTestCase;

class UserDbRepositoryTest extends AbstractDbTestCase
{
    protected UserDBRepository $repository;

    /**
     * Get UserDBRepository instance for use among all the tests
     *
     * @return void
     * @throws ReflectionException
     */
    public function setUp(): void
    {
        parent::setUp();
        $injector = new DependencyInjectionService();
        $this->repository = $injector->resolve(UserDBRepository::class);
    }

    /**
     * Test creating a new user
     *
     * @return void
     * @covers \SMSBump\Models\Repositories\UserDBRepository::crete
     */
    public function testCreateUser(): void
    {
        $this->assertEquals(0, $this->repository->count(), 'Check count before new user was created');
        $this->repository->crete(new User('test@test.com', '1234567', 'a534dfg'));
        $this->assertEquals(1, $this->repository->count(), 'Check count after new user was created');
    }

    /**
     * Test if user can be found by email
     *
     * @return void
     */
    public function testHasByEmail(): void
    {
        $this->markTestIncomplete();
    }
}
