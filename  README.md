# SMSBump
## _Registration form with OTP_

Directory structure is pretty much self-explanatory.
All the settings can be edited in the config.ini and .env files.

## Installation
#### Docker
The easieat metod is by using docker-compose. That will create all the necessary containers,
will create the DB structure, and will set the environment variables for you.
```sh
docker-compose up
```
Navigate to your server address in your preferred browser.

```sh
http://localhost:8080
```
If you need to make a Mysql connection on the host machine, the port will be 3309.

#### Manualy
Take the content from .env file and set it as environment variables.
The SQL file with the DB table structures can be found in init-db directory.
