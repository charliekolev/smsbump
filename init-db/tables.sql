CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `email` varchar(255) NOT NULL,
  `phone` char(12) NOT NULL,
  `otp` char(8) NOT NULL,
  `attempts` int(1) UNSIGNED DEFAULT 0 NOT NULL,
  `last_attempt` DATETIME,
  `verification_sent` DATETIME NOT NULL,
  `verified` DATETIME,
  UNIQUE KEY unique_email (email)
);

CREATE TABLE IF NOT EXISTS `user_verification_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_id` int(11) NOT NULL,
  `otp` char(12) NOT NULL,
  `status` ENUM('UNKNOWN', 'VERIFIED', 'WRONG CODE', 'AFTER LIMIT REACHED') DEFAULT 'UNKNOWN' NOT NULL,
  `created` DATETIME NOT NULL,
  CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `sms_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `phone` char(12) NOT NULL,
  `message` text NOT NULL,
  `sent` DATETIME NOT NULL
);

CREATE INDEX `sms_logs_phone` on `sms_logs` (`phone`);
