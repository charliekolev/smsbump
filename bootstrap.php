<?php

use SMSBump\Controllers\HomeController;
use SMSBump\Lib\DB\Database;
use SMSBump\Lib\DependencyInjection\DependencyInjectionService;
use SMSBump\Lib\Generic\DotEnv;
use SMSBump\Lib\Generic\Exceptions\ControllerException;
use SMSBump\Lib\Generic\ReflectionResolver;
use SMSBump\Lib\Generic\Session;
use SMSBump\Lib\Generic\View;
use SMSBump\Lib\SMS\DbSmsProviderStrategy;
use SMSBump\Lib\SMS\Sms;
use SMSBump\Models\Repositories\UserDBRepository;
use SMSBump\Models\Repositories\UserRepositoryInterface;

const SRC_DIRECTORY = __DIR__ . DIRECTORY_SEPARATOR . 'src';
const VIEWS_DIRECTORY = SRC_DIRECTORY . DIRECTORY_SEPARATOR . 'Views';
const TESTS_DIRECTORY = __DIR__ . DIRECTORY_SEPARATOR . 'tests';

// The project classes autoloader.
spl_autoload_register(static function ($class)
{
    $prefix = 'SMSBump\\';

    if (strpos($class, $prefix) === 0) {
        $file = SRC_DIRECTORY . DIRECTORY_SEPARATOR .
            str_replace('\\', DIRECTORY_SEPARATOR, substr($class, strlen($prefix))) . '.php';
    } elseif (strpos($class, 'Tests\\') === 0) {
        $file = TESTS_DIRECTORY . DIRECTORY_SEPARATOR . substr($class, strlen('Tests\\')) . '.php';
    }

    if (isset($file) && file_exists($file)) {
        require $file;
    }
});

// Load environment variables
$envFile = __DIR__ . '/' . ((!isset($_ENV['APP_ENV']) || $_ENV['APP_ENV'] !== 'test') ? '.env' : '.env.test');
(new DotEnv($envFile))->load();

define('ENVIRONMENT', $_ENV['APP_ENV'] ?? 'production');

/**
 * Generic function for showing an error page
 *
 * @param string $message
 * @param string $number
 * @param bool $userVisible
 *
 * @return void
 */
function smsbump_error_page(string $message = '', string $number = '404', bool $userVisible = false)
{
    http_response_code($number);

    $data = [
        'message' => ($userVisible || ENVIRONMENT !== 'production')
            ? $message
            : 'Unexpected issue. Call the administration please!'
    ];

    $viewFile = VIEWS_DIRECTORY . DIRECTORY_SEPARATOR . 'Main' . DIRECTORY_SEPARATOR . 'error.php';
    require VIEWS_DIRECTORY . DIRECTORY_SEPARATOR . 'Main' . DIRECTORY_SEPARATOR . 'page.php';
    exit();
}

// Initialize Dependency injection service
$diService = new DependencyInjectionService();

// Register config into dependency container as singleton
$diService->register('config', static function() {
    $configFile = __DIR__ . '/config.ini';

    return (
        is_readable($configFile) &&
        ($config = parse_ini_file($configFile, true))
    )
        ? (object)$config
        : (object)[];
});

// Register session into dependency container as singleton
$diService->register('session', static function() {
    $session = new Session();
    $session->start();
    return $session;
});

// Register database into dependency container as singleton
$diService->register(Database::class, static function() {
    $injector = new DependencyInjectionService();

    try {
        $config = $injector->get('config');

        if (isset($config->db['dsn'], $config->db['user'], $config->db['password'])) {
            $dbh = new PDO($config->db['dsn'], $config->db['user'], $config->db['password']);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return new Database($dbh);
        } else {
            throw new Exception('Missing DB settings.');
        }
    } catch (Exception $e) {
        smsbump_error_page($e->getMessage(), 500);
    }
});

// Register UserRepositoryInterface into dependency container for auto resolution
$diService->register(UserRepositoryInterface::class, static function() {
    $injector = new DependencyInjectionService();

    try {
        return $injector->resolve(UserDBRepository::class);
    } catch (ReflectionException $e) {
        smsbump_error_page($e->getMessage(), 500);
    }
});

// Register sms provider into dependency container
$diService->register(
    Sms::class,
    static function() {
        $injector = new DependencyInjectionService();

        try {
            $provider = $injector->resolve(DbSmsProviderStrategy::class);
            return new Sms($provider);
        } catch (ReflectionException $e) {
            smsbump_error_page($e->getMessage(), 500);
        }
    },
    false
);

// Simple routing
try {
    if (!empty($_GET['section']) && $_GET['action']) {
        // Load the controller file
        $controllerClass = '\SMSBump\Controllers\\' . ucfirst(strtolower($_GET['section'])) . 'Controller';

        if (!class_exists($controllerClass)) {
            smsbump_error_page('Not found!', 404);
        }

        $controller = $diService->resolve($controllerClass);

        if (!method_exists($controller, $_GET['action'])) {
            smsbump_error_page('Not found!', 404);
        }

        // Fire the action and get the result data
        $data = $controller->{$_GET['action']}();

        // Show the HTML
        View::render($_GET['action'], $data);
    } elseif (ENVIRONMENT !== 'test') {
        $controller = $diService->resolve(HomeController::class);

        // Show the HTML
        View::render('home', $controller->index());
    }
} catch (ControllerException $e) {
    smsbump_error_page(
        $e->getMessage(),
        500,
        $e->getCode() === ControllerException::VISIBILITY_USER
    );
} catch (Exception $e) {
    smsbump_error_page($e->getMessage(), 500);
}
